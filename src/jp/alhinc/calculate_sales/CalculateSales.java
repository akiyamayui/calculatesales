package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	
	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	
	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILENUMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String FIGURE_LENGTH_OVER = "合計金額が10桁を超えました";
	private static final String INCORRECT_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String INCORRECT_SALESCODE = "の支店コードが不正です";
	private static final String INCORRECT_PRODUCTSCODE = "の商品コードが不正です";
	
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> productNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> productSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,"支店","[0-9]{3}")) {
			return;
		}
		
		//商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, productNames, productSales,"商品","^[A-Za-z0-9]+${8}")) {
			return;
		}
		
// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		
		for(int i = 0; i < files.length; i++) {
			files[i].getName();
			String fileName = files[i].getName();
				
			if(files[i].isFile() && fileName.matches("[0-9]{8}.*.+rcd$")){
				rcdFiles.add(files[i]);
			}
		}
				
		for(int a = 0; a < rcdFiles.size() - 1; a++) {
			int former = Integer.parseInt(rcdFiles.get(a).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(a + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILENUMBER_ERROR);
				return;
			}
		}
				
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++ ) {
					
			try {
					FileReader fr = new FileReader(rcdFiles.get(i));       //ファイルごとの処理
					br = new BufferedReader(fr);
						
					String line;
					List<String> items = new ArrayList<>();
					while((line = br.readLine())!= null) {                  //行ごとの処理
						System.out.println(line);
						items.add(line);
					}	
						
					if(items.size() != 3) {
						System.out.println(rcdFiles.get(i).getName() + INCORRECT_FORMAT);
						return;
					}
					if(!branchNames.containsKey(items.get(0))){
						System.out.println(rcdFiles.get(i).getName() + INCORRECT_SALESCODE);
						return;
					}
					if(!productNames.containsKey(items.get(1))) {
						System.out.println(rcdFiles.get(i).getName() + INCORRECT_PRODUCTSCODE);
						return;
					}
					
					if(!items.get(2).matches("^[0-9]*$")) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
					
					long fileSales = Long.parseLong(items.get(2));
					Long saleAmount = branchSales.get(items.get(0)) +  fileSales;
					Long productAmount = productSales.get(items.get(1)) + fileSales;
										
					if(saleAmount >= 10000000000L || productAmount >= 10000000000L) {
						System.out.println(FIGURE_LENGTH_OVER);
						return;
					}
					branchSales.put(items.get(0),saleAmount);
					productSales.put(items.get(1),productAmount);
							
			}catch(IOException ex) {
				System.out.println(UNKNOWN_ERROR);
			}finally {
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
					
			}
			
		// 支店別集計ファイル書き込み処理
			if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
				return;
			}
			
		//商品別集計ファイルの書き込み処理
			if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, productNames, productSales)) {
				return;
			}
		}
	}

	/**
	 * 支店定義ファイル・商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String Error, String Regex) {
		BufferedReader br = null;
		
		try {
			File file = new File(path,fileName);
			if(!file.exists()) {
				System.out.println(Error + FILE_NOT_EXIST);
				return false;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");									
				
				if((items.length != 2) || (!items[0].matches(Regex))){			//支店定義ファイルは数字３桁、商品定義ファイルは英文字８桁
					System.out.println(Error + INCORRECT_FORMAT);
					return false;
				}
				
				System.out.println(line);
				
				Names.put(items[0],items[1]);
				Sales.put(items[0],(long) 0);
				
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * 支店別集計ファイル・商品別集計ファイルの書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter br = null;

		try {
			File file = new File(path,fileName);
			FileWriter fr = new FileWriter(file);
			br = new BufferedWriter(fr);
			
			String salesName;
			Long salesTotal;
			for(String key :Names.keySet()) {     //支店コード・商品コードの取得
				salesName = Names.get(key);
				salesTotal = Sales.get(key);

				//書き込む
				br.write(key + ","+ salesName + "," + salesTotal);
				br.newLine();
			}
			
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
